@extends('layouts.app')

@section('title', 'Management')

@section('content')
<div class="container">
    <h2 class="my-4 text-center h2">Management</h2>
    <table class="table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Description</th>
                <th>View Count</th>
                <th>Is Discount</th>
                <th>%</th>
                <th><button class="btn btn-success" id="addRow">+</button></th>
            </tr>
        </thead>
        <tbody>
            <tr style="display: none;" id="newRow">
                <form action="/product/create" method="POST">
                    <td><input type="text" name="name" required></td>
                    <td><input type="number" name="price" required min="0"></td>
                    <td><input type="text" name="description"></td>
                    <td><input type="text" name="viewCount" value="0" readonly></td>
                    <td>
                        <input type="checkbox" name="isDiscount">
                    </td>
                    <td><input type="number" name="discountAmount" value="0" min="0" max="100"></td>
                    <td><button class="btn btn-success">Create</button></td>
                </form>
            </tr>
            @foreach ($products as $product)
            <tr>
                <form action="/product/update/{{$product->id}}" method="POST">
                    <td><input type="text" name="name" value="{{ $product->name }}"></td>
                    <td><input type="number" name="price" value="{{ $product->price }}" min="0"></td>
                    <td><input type="text" name="description" value="{{ $product->description }}"></td>
                    <td><input type="number" name="viewCount" value="{{ $product->viewCount }}"></td>
                    <td>
                        <input type="checkbox" name="isDiscount" {{ $product->isDiscount ? 'checked' : '' }}>
                    </td>
                    <td><input type="number" name="discountAmount" value="{{ $product->discountAmount }}" min="0" max="100"></td>
                    <td>
                        <button class="btn btn-primary">Save</button>
                        <a href="/product/{{ $product->id }}" role="button" class="btn btn-primary">Info</a>
                        <a href="/product/delete/{{ $product->id }}" role="button" class="btn btn-danger delete-btn">Delete</a>
                    </td>
                </form>
            </tr>
            @endforeach
        </tbody>
    </table>
    <div class="mt-4 mb-4">
        {{ $products->links() }}
    </div>
</div>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        const addRowButton = document.getElementById('addRow');
        const newRow = document.getElementById('newRow');
        const tbody = document.querySelector('table tbody');

        addRowButton.addEventListener('click', function(event) {
            event.preventDefault();
            newRow.style.display = 'table-row';
        });
    });
</script>
@endsection