@extends('layouts.app')

@section('title', 'Product Detail')

@section('content')
<div class="container mx-auto px-4 py-8">
    <h1 class="h1 text-center">Product Detail</h1>
    <div class="card d-flex flex-column h-100  text-center">
        <div class="card-body">
            <h5 class="card-title h5">Name: {{ $product->name }}</h5>
            <p class="card-text">Price: $ {{ $product->price }}</p>
            <p class="card-text text-muted">@if ($product->isDiscount)
                Discount: {{ $product->discountAmount }}%
                @endif</p>
            <p class="card-text text-muted">View: {{ $product->viewCount }}</p>
            <p class="card-text text-muted">{{ $product->description }}</p>
        </div>
    </div>
    <div class="mt-4">
        <h4 class="h4">Comment</h4>
        @foreach ($comments as $comment)
        <div class="flex w-full justify-between border rounded-md mt-3">
            <div class="p-3">
                <div class="flex gap-3 items-center">
                    <img src="https://avatars.githubusercontent.com/u/22263436?v=4" class="object-cover w-10 h-10 rounded-full border-2 border-emerald-400 shadow-emerald-400">
                    <h3 class="font-bold">
                        {{ $comment->user->name }}
                        <br>
                        <span class="text-sm text-gray-400 font-normal">{{ $comment->created_at}}</span>
                    </h3>
                </div>
                <p class="text-gray-600 mt-2">
                    {{ $comment->content }}
                </p>
                <button class="text-right text-blue-500 reply-btn" data-comment-id="{{ $comment->id }}">Reply</button>
            </div>
        </div>

        <form class="reply-form" action="/reply" method="post" style="display: none;">
            <div class="text-gray-300 font-bold pl-14">|</div>
            <div class="ml-5">
                <textarea class="bg-gray-100 rounded border border-gray-400 leading-normal resize-none w-full h-20 py-2 px-3 font-medium placeholder-gray-400 focus:outline-none focus:bg-white" name="content" placeholder="Reply" required></textarea>
                <input type="hidden" name="comment_id" class="comment-id-input" value="">
            </div>

            <div class="w-full flex justify-end">
                <input type="submit" class="px-2.5 py-1.5 rounded-md text-white text-sm bg-indigo-500 text-lg" value='Reply'>
            </div>
        </form>

        <!-- Replies Container -->
        @foreach ($comment->replies as $reply)
        <div class="text-gray-300 font-bold pl-14">|</div>
        <div class="flex justify-between border ml-5 rounded-md">
            <div class="p-3">
                <div class="flex gap-3 items-center">
                    <img src="https://avatars.githubusercontent.com/u/22263436?v=4" class="object-cover w-10 h-10 rounded-full border-2 border-emerald-400 shadow-emerald-400">
                    <h3 class="font-bold">
                        {{ $reply->user->name }}
                        <br>
                        <span class="text-sm text-gray-400 font-normal">{{ $reply->created_at }}</span>
                    </h3>
                </div>
                <p class="text-gray-600 mt-2">
                    {{ $reply->content }}
                </p>
            </div>
        </div>
        @endforeach

        <!-- END Replies Container -->

        @endforeach

        <form action="/comment" method="POST">
            <div class="w-full px-3 mb-2 mt-6">
                <textarea class="bg-gray-100 rounded border border-gray-400 leading-normal resize-none w-full h-20 py-2 px-3 font-medium placeholder-gray-400 focus:outline-none focus:bg-white" name="content" placeholder="Comment" required></textarea>
                <input type="hidden" name="product_id" value="{{ $product->id }}">
            </div>

            <div class="w-full flex justify-end px-3 my-3">
                <input type="submit" class="px-2.5 py-1.5 rounded-md text-white text-sm bg-indigo-500 text-lg" value='Post Comment'>
            </div>
        </form>
    </div>

</div>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        const replyButtons = document.querySelectorAll('.reply-btn');
        const replyForms = document.querySelectorAll('.reply-form');
        const commentIdInputs = document.querySelectorAll('.comment-id-input');

        replyButtons.forEach((button, index) => {
            button.addEventListener('click', function() {
                const replyForm = replyForms[index];
                const commentIdInput = commentIdInputs[index];
                replyForm.style.display = 'block';
                replyForm.querySelector('textarea').focus();
                commentIdInput.value = this.getAttribute('data-comment-id');
            });
        });
    });
</script>
@endsection