@extends('layouts.app')

@section('title', 'Product List')

@section('content')
<div class="container">
    <h2 class="my-4 text-center h2">Product List</h2>
    <div class="row">
        @foreach ($products as $product)
        <div class="col-md-4 mb-4">
            <div class="card d-flex flex-column h-100">
                <a href="/product/{{ $product->id }}">
                    <div class="card-body d-flex justify-content-between align-items-center">
                        <div>
                            <h5 class="card-title h5">{{ $product->name }}</h5>
                            <p class="card-text">$ {{ $product->price }}</p>
                        </div>
                        <p class="card-text">
                            @if ($product->isDiscount)
                            Discount: {{ $product->discountAmount }}%
                            @endif
                        </p>
                    </div>
                </a>
            </div>
        </div>
        @endforeach
    </div>
    <div class="mt-4 mb-4">
        {{ $products->links() }}
    </div>
</div>
@endsection