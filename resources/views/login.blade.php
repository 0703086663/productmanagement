<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Login</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.2/dist/js/bootstrap.min.js"></script>

</head>

<body>
  <div class="container mt-5">
    <form method="POST" action="/api/login">
      <div class="form-group">
        <label for="email">User name</label>
        <input type="text" class="form-control" id="email" name="email" placeholder="Enter email" required>
      </div>
      <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    <div class="mt-4">
      <p class="text-muted h6">
        Admin: admin@gmail.com |
        Password: admin
      </p>
      <hr>
      <p class="text-muted h6">
        Admin: user@gmail.com |
        Password: user
      </p>
    </div>
  </div>
</body>

</html>