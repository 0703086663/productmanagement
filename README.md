
# Setting up to run project

Welcome to my Lumen project! Here are the steps you need to follow to run this project:

1. **Install Dependencies:**
   Open a terminal/command prompt and navigate to the project directory. Run the following command to install project dependencies using Composer:

   ```bash
   composer install
2. **Environment Configuration:**
Create a copy of the .env.example file and rename it to .env:

    ```bash
    cp .env.example .env
*Open the .env file and update the necessary configuration values, such as database settings and other environment-specific variables.*

3. **Generate Application Key:**
Run the following command to generate a unique application key for your project:

    ```bash
    php artisan key:generate
4. **Run Migrations:**
Run the migrations to create the necessary database tables:

    ```bash
    php artisan migrate
5. **Run Seeds:**
Run this to get example data for your database.
    ```bash
    php artisan db:seed
6. **Config for jwt:**
This project use jwt to authentication user so there are some changes needs to be done to run correctly.
Open your **auth.php** in **vendor/laravel/lumen-framework/config** and change content to it.
    ```php
    <?php
    return [
        'defaults' => [
            'guard' => env('AUTH_GUARD', 'api'),
        ],

        'guards' => [
            'api' => [
                'driver' => 'jwt',
                'provider' => 'users',
            ],
        ],

        'providers' => [
            'users' => [
                'driver' => 'eloquent',
                'model' => \App\Models\User::class
            ]
        ],

        'passwords' => [
            //
        ],
    ];
    
7. **Start the Development Server:**
Start the development server to run your Lumen project locally:

    ```bash
    php -S localhost:8000 -t public
The application will now be accessible at http://localhost:8000.





