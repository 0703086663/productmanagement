<?php

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request;

class VerifyJWTToken
{
    public function handle(Request $request, Closure $next)
    {
        // Retrieve the JWT token from the cookie
        $jwtToken = $request->cookie('jwt_token');

        if ($jwtToken) {
            // Attempt to validate the JWT token
            try {
                JWTAuth::setToken($jwtToken)->authenticate();
            } catch (\Exception $e) {
                // Token validation failed, handle the error (e.g., return a 401 Unauthorized response)
                return response()->json(['message' => 'Unauthorized'], 401);
            }
        } else {
            // No token found, handle the error (e.g., return a 401 Unauthorized response)
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        // Token is valid, proceed to the next middleware or route handler
        return $next($request);
    }
}
