<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Cookie;
use App\Models\User;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'refresh', 'logout']]);
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $credentials = $request->only(['email', 'password']);

        if (!$token = Auth::attempt($credentials)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        $cookie = new Cookie('jwt_token', $token, time() + Auth::factory()->getTTL() * 60);

        return redirect('/product')->withCookie($cookie);
    }

    public function logout()
    {
        $cookie = new Cookie('jwt_token', null, time() - 3600);
        return redirect('/')->withCookie($cookie);
    }

    public function refresh()
    {
        return $this->respondWithToken(Auth::refresh());
    }

    protected function respondWithToken($token)
    {
        $response = response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'user' => auth()->user(),
            'expires_in' => Auth::factory()->getTTL() * 60 * 24
        ]);

        $cookie = new Cookie('jwt_token', $token, time() + Auth::factory()->getTTL() * 60);
        $response->headers->setCookie($cookie);

        return $response;
    }
}
