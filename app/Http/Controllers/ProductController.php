<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Comment;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Retrieve 6 products each page.
     *
     * @return Response
     */
    public function index()
    {
        $products = Product::paginate(6);
        return view('products.index', compact('products'));
    }

    /**
     * Retrieve detail of one product.
     *
     * @param int $id
     * @return Response
     */
    public function detail(int $id)
    {
        $product = Product::findOrFail($id);

        // Increment viewCount
        $product->viewCount++;
        $product->save();

        $comments = Comment::with(['replies' => function ($query) {
            $query->with('user')->orderBy('created_at', 'desc');
        }])
            ->where('product_id', $id)
            ->orderBy('created_at', 'desc')
            ->get();


        return view('products.detail', compact('product', 'comments'));
    }


    /**
     * Retrieve all the products.
     *
     * @return Response
     */
    public function management()
    {
        $products = Product::orderBy('updated_at', 'desc')->paginate(6);
        return view('products.management', compact('products'));
    }


    /**
     * Edit product by id.
     *
     * @param Int $id
     * @return Response
     */
    public function update(int $id, Request $request)
    {
        $product = Product::findOrFail($id);

        // Validate the input data
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'price' => 'required|numeric',
            'description' => 'string',
            'viewCount' => 'required|integer',
            'isDiscount' => 'string',
        ]);

        // Update the product details
        $product->update([
            'name' => $request->input('name'),
            'price' => $request->input('price'),
            'description' => $request->input('description'),
            'viewCount' => $request->input('viewCount'),
            'isDiscount' => $request->has('isDiscount'),
        ]);

        return redirect('/management');
    }

    /**
     * Delete product by id.
     *
     * @param Int $id
     * @return Response
     */
    public function delete(int $id)
    {
        $product = Product::findOrFail($id);

        $product->delete();

        return redirect('/management');
    }

    /**
     * Create a new product.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'price' => 'required|numeric',
            'description' => 'string',
            'viewCount' => 'integer',
            'isDiscount' => 'string',
        ]);

        Product::create([
            'name' => $request->input('name'),
            'price' => $request->input('price'),
            'description' => $request->input('description'),
            'viewCount' => 0,
            'isDiscount' => $request->has('isDiscount'),
        ]);

        return redirect('/management');
    }
}
