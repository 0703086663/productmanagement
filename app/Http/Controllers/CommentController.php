<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;
use Illuminate\Support\Facades\Auth;


class CommentController extends Controller
{
    /**
     * 
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required|exists:products,id',
            'content' => 'required|string|max:255',
        ]);

        $comment = Comment::create([
            'product_id' => $request->product_id,
            'user_id' => Auth::user()->id,
            'content' => $request->content,
        ]);

        return redirect()->to(app('request')->headers->get('referer'));
    }
}
