<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reply;
use Illuminate\Support\Facades\Auth;


class ReplyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'comment_id' => 'required|exists:comments,id',
            'content' => 'required|string|max:255',
        ]);

        Reply::create([
            'comment_id' => $request->comment_id,
            'user_id' => Auth::user()->id,
            'content' => $request->content,
        ]);

        return redirect()->to(app('request')->headers->get('referer'));
    }
}
