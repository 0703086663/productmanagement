<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Support\Str;
use App\Models\Reply;

class ReplyTableSeeder extends Seeder
{
    public function run()
    {
        // Get all comments and users
        $comments = Comment::all();
        $users = User::all();

        // Loop through each comment
        foreach ($comments as $comment) {
            // Create random replies for each comment
            for ($i = 0; $i < rand(0, 2); $i++) {
                Reply::create([
                    'user_id' => $users->random()->id,
                    'comment_id' => $comment->id,
                    'content' => Str::random(30),
                ]);
            }
        }
    }
}
