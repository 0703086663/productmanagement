<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Faker\Factory as Faker;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Generate random data
        $faker = Faker::create();
        for ($i = 0; $i < 15; $i++) {
            $isDiscount = $faker->boolean;
            $discountAmount = $isDiscount ? $faker->randomFloat(2, 5, 30) : null;

            DB::table('products')->insert([
                'name' => $faker->word,
                'description' => $faker->sentence,
                'viewCount' => $faker->numberBetween(0, 100),
                'price' => $faker->numberBetween(0, 100000),
                'isDiscount' => $isDiscount,
                'discountAmount' => $faker->numberBetween(0, 100),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
