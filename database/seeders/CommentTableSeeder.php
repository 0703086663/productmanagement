<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Support\Str;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Get all products and users
        $products = Product::all();
        $users = User::all();

        // Loop through each product
        foreach ($products as $product) {
            // Create random comments
            for ($i = 0; $i < rand(2, 5); $i++) {
                Comment::create([
                    'user_id' => $users->random()->id,
                    'product_id' => $product->id,
                    'content' => Str::random(30),
                ]);
            }
        }
    }
}
