<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', 'HomeController@index');

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->post('/login', 'AuthController@login');
    $router->post('/refresh', 'AuthController@refresh');
});
$router->get('/logout', 'AuthController@logout');

$router->group(['middleware' => 'jwt.verify'], function ($router) {
    $router->get('/product', 'ProductController@index');
    $router->get('/product/{id}', 'ProductController@detail');
    $router->group(['middleware' => 'role:admin'], function ($router) {
        $router->get('/management', 'ProductController@management');
        $router->post('/product/update/{id}', 'ProductController@update');
        $router->get('/product/delete/{id}', 'ProductController@delete');
        $router->post('/product/create', 'ProductController@create');
    });
    // Comment
    $router->post('/comment', 'CommentController@store');
    $router->post('/reply', 'ReplyController@store');
});
